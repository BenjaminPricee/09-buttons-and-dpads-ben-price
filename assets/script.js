const canvas = document.getElementById('the_canvas').getContext('2d');
var score = document.getElementById("score");
var scoreCount = 0;

function gameObject(image, x, y, width, height) {
    this.image = image;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let image = new Image();
image.src = "assets/Player.png";

function KeyInput(input) {
    this.action = input;
}
// -------------------------------------------------------------------------------------------------------------
let Width = 133;
let Height = 200
let frameX = 0;
let frameY = 0;
let frameXDelay = 0;

let keyInput = new KeyInput("None");
let player = new gameObject(image, 0, 0, Width, Height);

// -------------------------------------------------------------------------------------------------------------

var max = 100;
var val = 100;

// -------------------------------------------------------------------------------------------------------------
let upButton = document.getElementById('UP');
let downButton = document.getElementsByClassName('DOWN');
let leftButton = document.getElementsByClassName('LEFT');
let rightButton = document.getElementsByClassName('RIGHT');

function HandleInput(event) {
    if (event.type === 'mousedown') {
        if (event.target.className === 'DOWN') {
            keyInput = new KeyInput("Down");
        }
        if (event.target.className === 'UP') {
            keyInput = new KeyInput("Up");
        }
        if (event.target.className === 'LEFT') {
            keyInput = new KeyInput("Left");
        }
        if (event.target.className === 'RIGHT') {
            keyInput = new KeyInput("Right");
        }
    }

    if (event.type === 'mouseup') {
        keyInput = new KeyInput("None");
    }
}

// -------------------------------------------------------------------------------------------------------------

var dynamic = nipplejs.create({
    color: 'grey',
    zone: document.getElementById('JoyStick'),
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
        //console.log("direction up");
        keyInput = new KeyInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        keyInput = new KeyInput("Down");
    });
    nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        keyInput = new KeyInput("Left");
    });
    nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        keyInput = new KeyInput("Right");
    });
    nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        keyInput = new KeyInput("None");
    });
});


// -------------------------------------------------------------------------------------------------------------

function update() {
    if (keyInput.action === "Up") {
        frameY = 1;
        player.y -= 3;
        frameXDelay += 1;
    } else if (keyInput.action === "Down") {
        frameY = 0;
        player.y += 3;
        frameXDelay += 1;
    } else if (keyInput.action === "Left") {
        frameY = 2;
        player.x -= 3;
        frameXDelay += 1;
    } else if (keyInput.action === "Right") {
        frameY = 3;
        player.x += 3;
        frameXDelay += 1;
    } else if (keyInput.action === 'None') {
        frameX = 0;
    }

    if (frameXDelay > 10) {
        frameXDelay = 0;
        frameX++;
        scoreCount += .5;
    }

    if (frameX >= 4) {
        frameX = 0;
    }

    val -= 0.025;
}

// -------------------------------------------------------------------------------------------------------------
function draw() {
    canvas.clearRect(0, 0, 1000, 1000);

    canvas.drawImage(player.image,
        frameX * Width,
        frameY * Height,
        Width,
        Height,
        player.x,
        player.y,
        player.width,
        player.height);
}

// -------------------------------------------------------------------------------------------------------------
function drawHealthbar() {
    var width = 800;
    var height = 20;
    
    // Draw the background
    canvas.fillStyle = "#000000";
    canvas.fillRect(0, 0, width, height);

    // Draw the fill
    canvas.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(val / max, 0), 1);
    canvas.fillRect(0, 0, fillVal * width, height);
}
// -------------------------------------------------------------------------------------------------------------
function scoreUpdate() {
    let scoreFix = scoreCount;
    let scoreDisplay = Math.round(scoreFix);
    score.innerHTML = "Fitbit steps = " + scoreDisplay;
}
// -------------------------------------------------------------------------------------------------------------


function GameLoop() {
    window.requestAnimationFrame(GameLoop);
    //HandleInput();
    update();
    draw();
    drawHealthbar();
    scoreUpdate();
}

// -------------------------------------------------------------------------------------------------------------

window.requestAnimationFrame(GameLoop);

window.addEventListener('mousedown', HandleInput);
window.addEventListener('mouseup', HandleInput);
